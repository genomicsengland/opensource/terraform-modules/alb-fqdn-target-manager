resource "aws_cloudwatch_event_rule" "fqdn_resolver" {
  name                = var.name
  description         = "${var.name} Scheduler"
  schedule_expression = var.cron_schedule
  is_enabled          = true
}

resource "aws_cloudwatch_event_target" "fqdn_resolver" {
  target_id = var.name
  arn       = aws_lambda_function.fqdn_resolver.arn
  rule      = aws_cloudwatch_event_rule.fqdn_resolver.name
}

data "archive_file" "fqdn_resolver" {
  type        = "zip"
  source_dir  = "${path.module}/source/"
  output_path = "${path.module}/.terraform/${var.name}.zip"
}

#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "fqdn_resolver" {
  function_name    = var.name
  filename         = data.archive_file.fqdn_resolver.output_path
  source_code_hash = data.archive_file.fqdn_resolver.output_base64sha256
  role             = aws_iam_role.fqdn_resolver.arn
  runtime          = "python3.9"
  handler          = "lambda.handler"
  architectures    = ["arm64"]
  memory_size      = 256
  timeout          = 300
  environment {
    variables = {
      FQDN_TAG = var.fqdn_tag
    }
  }
}

#tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "fqdn_resolver" {
  name              = "/aws/lambda/${aws_lambda_function.fqdn_resolver.function_name}"
  retention_in_days = var.log_retention_in_days
}

resource "aws_iam_role" "fqdn_resolver" {
  name = var.name

  assume_role_policy = data.aws_iam_policy_document.lambda_assume.json
  inline_policy {
    name   = "permissions"
    policy = data.aws_iam_policy_document.fqdn_resolver.json
  }
  permissions_boundary = var.permission_boundary
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

#tfsec:ignore:aws-iam-no-policy-wildcards
data "aws_iam_policy_document" "fqdn_resolver" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["*"]
  }

  statement {
    actions = [
      "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:DescribeTargetHealth",
    ]
    resources = ["*"]
  }
}

resource "aws_lambda_permission" "fqdn_resolver" {
  statement_id  = "ExecFromEvent"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.fqdn_resolver.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.fqdn_resolver.arn
}